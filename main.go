package main

import (
	"crypto/md5"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"sync"
)

func hash(data []byte) string {
	return fmt.Sprintf("%x", md5.Sum(data))
}

func makeRequest(url string) (string, error) {
	resp, err := http.Get(url)

	if err != nil {
		log.Printf("could not get from %s Error: %v\n", url, err)
		return "", err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		log.Printf("could not read body: %v\n", err)
		return "", err
	}

	return hash(body), nil
}

func worker(queue <-chan string) {
	for url := range queue {
		resp, err := makeRequest(url)
		if err != nil {
			log.Printf("error while making the request: %v\n", err)
			continue
		}
		fmt.Println(url, resp)
	}
}

func main() {
	//Parse flags
	parallel := flag.Int("parallel", 10, "How many request can run in parallel")
	flag.Parse()

	//Parse url args
	urls := flag.Args()

	//Create buffered channel to be used as a queue
	queue := make(chan string, len(urls))

	//Fill the urls queue
	for _, url := range urls {
		//Add missing protocol
		if !(strings.HasPrefix(url, "http://") || strings.HasPrefix(url, "https://")) {
			url = fmt.Sprintf("https://%s", url)
		}
		queue <- url
	}
	close(queue)

	//Create a waitgroup
	var wg sync.WaitGroup
	//Start the number of workers for parallel work
	for i := 0; i < *parallel; i++ {
		wg.Add(1)
		go func() {
			worker(queue)
			wg.Done()
		}()
	}

	wg.Wait()
}
