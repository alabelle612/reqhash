package main

import (
	"net/http"
	"testing"
)

//Note: I would have used testify/assert if I were allowed to use 3rd party packages

func Test_hash(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name string
		data []byte
		hash string
	}{
		{
			name: "hash returns expected hash value",
			data: []byte("This is a test"),
			hash: "ce114e4501d2f4e2dcea3e17b546f339",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if hash := hash(tt.data); hash != tt.hash {
				t.Errorf("GetMD5Hash() = %v, want %v", hash, tt.hash)
			}
		})
	}
}

func Test_makeRequest(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name           string
		url            string
		expectedResult string
		expectedErrMsg string
		handlerStub    http.HandlerFunc
	}{
		{
			name:           "Error making get request to url",
			url:            "fake.url",
			expectedResult: "",
			expectedErrMsg: `Get fake.url: unsupported protocol scheme ""`,
		},
		{
			//Normally I would not do this since the test will fail if the response changes
			name:           "Request against a real url works as intended",
			url:            "http://adjust.com",
			expectedResult: "66c3a7e502e2b417520f3dc852aabc88",
			expectedErrMsg: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			res, err := makeRequest(tt.url)

			if res != tt.expectedResult {
				t.Errorf("makeRequest() = %v, want %v", res, tt.expectedResult)
			}

			if err != nil && err.Error() != tt.expectedErrMsg {
				t.Errorf("makeRequest() error = %v, expected %v", err, tt.expectedErrMsg)
			}
		})
	}
}

func Test_worker(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name  string
		queue chan string
	}{
		{
			name: "the worker ranges over each item in the queue",
			queue: func() chan string {
				c := make(chan string, 2)

				defer close(c)

				c <- "test 1"
				c <- "test 2"

				return c
			}(),
		},
		{
			name: "the worker runs through without errors",
			queue: func() chan string {
				c := make(chan string, 2)

				defer close(c)

				c <- "http://adjust.com"

				return c
			}(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			worker(tt.queue)
		})
	}
}

func Test_main(t *testing.T) {
	//Just test that main runs through without errors
	main()
}
