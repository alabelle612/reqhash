# Usage
`bin/reqhash [-parallel (Optional)] [urls]`

**Example**

`bin/reqhash -parallel 2 https://google.com https.gmx.de`

**_Note:_**
The bin folder contains executables for amd64 for Mac and Windows only.


